package io.treats.opensesame;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.io.Closeables;
import com.parse.ParseObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DoorOpenIntent extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_OPEN = "io.treats.opensesame.action.OPEN";

    private static final String DOOR_ID = "io.treats.opensesame.extra.DOOR_ID";

    private static final Object[] lock = new Object[0];

    private boolean stopTests =false;

    public DoorOpenIntent() {
        super("DoorOpenIntent");
    }


    public void stopTests()
    {
        stopTests = true;
    }

    public void startTests(){
        stopTests = false;
        Thread testThread = new Thread(new Runnable() {
            @Override
            public void run() {

                Random r = new Random();
                try {
                    while(true) {
                        if(r.nextFloat() < 0.4) handleActionFoo("1");
                        if(r.nextFloat() < 0.4) handleActionFoo("2");
                        if(r.nextFloat() < 0.4) handleActionFoo("3");
                        if(r.nextFloat() < 0.4) handleActionFoo("4");
                        if(r.nextFloat() < 0.4) handleActionFoo("5");
                        if(r.nextFloat() < 0.4) handleActionFoo("6");
                        if(r.nextFloat() < 0.4) handleActionFoo("7");
                        if(r.nextFloat() < 0.4) handleActionFoo("8");
                        if(r.nextFloat() < 0.4) handleActionFoo("9");
                        if(r.nextFloat() < 0.4) handleActionFoo("10");
                        if(r.nextFloat() < 0.4) handleActionFoo("11");
                        if(r.nextFloat() < 0.4) handleActionFoo("12");
                        if(r.nextFloat() < 0.4) handleActionFoo("13");

                        if(stopTests)
                        {
                            return;
                        }

                        Thread.sleep(45 * 1000);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        testThread.start();

    }




    @Override
    protected void onHandleIntent(Intent intent) {
        l("Receiving intent " + String.valueOf(intent));
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_OPEN.equals(action)) {
                final String doorId = intent.getStringExtra(DOOR_ID);
                handleActionFoo(doorId);
            }
        }

    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String doorId) {

        int idForJson = Integer.parseInt(doorId) - 1 + 40;

        l("Opening Door " + doorId + "(" + idForJson + ")");

        String open = String.format("%02d%02d%02d\n", idForJson, idForJson, idForJson);

        synchronized(lock) {

            HttpURLConnection urlConnection = null;
            try {
                String urlString = "http://192.168.43.56/?cmd=" + open;

                l("Request: " + urlString);

                URL url = new URL(urlString);

                urlConnection = (HttpURLConnection) url.openConnection();
                BufferedInputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String content = CharStreams.toString(new InputStreamReader(in, Charsets.UTF_8));
                Closeables.closeQuietly(in);

                l("Response: " + content);
                Thread.sleep(1500);
            }
            catch(Exception e)
            {
                e(e.getMessage());
            } finally {
                if(urlConnection != null) {
                    urlConnection.disconnect();
                }
            }


        }

    }



    private void l(Object msg) {
        Log.d("TREATS", ">==< " + msg.toString() + " >==<");
        ParseObject testObject = new ParseObject("treats-opensesame");
        testObject.put("msg", msg.toString());
        testObject.put("type", "debug");
        testObject.put("time", System.currentTimeMillis());
        testObject.saveInBackground();
    }
    private void e(Object msg) {
        Log.e("TREATS", ">==< " + msg.toString() + " >==<");
        ParseObject testObject = new ParseObject("treats-opensesame");
        testObject.put("msg", msg.toString());
        testObject.put("type", "error");
        testObject.put("time", System.currentTimeMillis());
        testObject.saveInBackground();
    }


}
