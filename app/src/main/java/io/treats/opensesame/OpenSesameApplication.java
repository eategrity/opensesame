package io.treats.opensesame;

import android.app.Application;
import android.content.Context;

import com.parse.Parse;

/**
 * Created by doublemalt on 1/3/16.
 */
public class OpenSesameApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        Parse.initialize(this);

        OpenSesameApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return OpenSesameApplication.context;
    }
}
