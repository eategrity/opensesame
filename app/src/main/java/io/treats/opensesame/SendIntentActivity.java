package io.treats.opensesame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SendIntentActivity extends AppCompatActivity {
    private static final String ACTION_OPEN = "io.treats.opensesame.action.OPEN";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_intent);
    }

    public void startTests(View view)
    {
        new DoorOpenIntent().startTests();
    }

    public void stopTests(View view)
    {
        new DoorOpenIntent().stopTests();
    }


 }
